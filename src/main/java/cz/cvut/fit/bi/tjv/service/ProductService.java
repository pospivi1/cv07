package cz.cvut.fit.bi.tjv.service;

import cz.cvut.fit.bi.tjv.dao.ProductDao;
import cz.cvut.fit.bi.tjv.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductDao productDao;

    @Transactional
    public void add(Product product) {
        productDao.persist(product);
    }

    @Transactional
    public void addAll(Collection<Product> products) {
        for (Product product : products) {
            productDao.persist(product);
        }
    }

    @Transactional(readOnly = true)
    public List<Product> listAll() {
        return productDao.findAll();
    }
}